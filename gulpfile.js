const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require ('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const rename = require("gulp-rename");
const plumber = require('gulp-plumber');
const pug = require('gulp-pug');
const imagemin  = require('gulp-imagemin');
const changed = require('gulp-changed');
const browserSync = require('browser-sync').create();

function estilos() { 
  return gulp.src('./src/scss/**/*.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest('./dist/css'))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(autoprefixer ({
    browsers: ['last 2 versions']
  }))
  .pipe(cssnano({
    core:true
  }))
  .pipe(gulp.dest('./dist/css'))
  .pipe(browserSync.stream());

 }

 function vistas() {
   return gulp.src('./src/pug/*.pug')
   .pipe(plumber())
   .pipe(pug({
     pretty: true
   }))
   .pipe(gulp.dest('./dist/'))
   .on('end', browserSync.reload);
 }

function imagenes() {
  return gulp.src('./src/img/*')
  .pipe(changed('./dist/img/'))
  .pipe(imagemin())
  .pipe(gulp.dest('./dist/img/'));
}

 function watch() {
   browserSync.init({
     server: {
       baseDir: './dist/'
     } 
   });

   gulp.watch('./src/scss/**/*.scss', estilos);
   gulp.watch('./src/pug/**/*.pug', vistas);
   gulp.watch('./src/img/*', imagenes);
   gulp.watch('./dist/*.html').on('change', browserSync.reload);
 }

 exports.estilos = estilos;
 exports.vistas = vistas;
 exports.imagenes = imagenes;
 exports.watch = watch;