const botonMenu = document.querySelector('.boton-menu');
const navegacion = document.querySelector('.navegacion');
const navegacionLista = document.querySelector('.navegacion__lista');
const navegacionItem = document.querySelectorAll('.navegacion__item');

let showMenu = false;

botonMenu.addEventListener('click', toggleMenu);

function toggleMenu() {
    if (!showMenu) {
        botonMenu.classList.add('cerrar');
        navegacion.classList.add('mostrar');
        navegacionLista.classList.add('mostrar');
        navegacionItem.forEach(item => item.classList.add('mostrar'));
        
        showMenu = true;
    } else {
        botonMenu.classList.remove('cerrar');
        navegacion.classList.remove('mostrar');
        navegacionLista.classList.remove('mostrar');
        navegacionItem.forEach(item => item.classList.remove('mostrar'));

        showMenu = false;
    }
}